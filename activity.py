# Create 5 variables
name = "Joseph"
age = 29
occupation = "Engineer"
movie = "Shawshank Redemption"
rating = 95.8

print(f"I am  + {name}  , and I am {age} years old, I work as an {occupation}, and my rating for {movie} is {rating}%")

# Create 3 variables
num1 = 25
num2 = 6
num3 = 30

print(num1 * num2)
print(num1 < num3)
print(num3 + num2)
